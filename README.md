# Docker Workflow


**To install on Ubuntu:**
*Windows is a bit of a fuck around*

-  `sudo apt-get update`
-  sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
-  `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`
-  sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
-  `sudo apt-get update`
-  `sudo apt-get install docker-ce`

*Docker should be installed on the machine.*
run `sudo docker run hello-world` to verify

**General Workflow:**

- Prepare Dockerfile to create image and .dockerignore files
- `sudo docker build -t give-the-image-a-tag .` **This builds an image from the instructions in the Docker container. The final full stop means this directory**
- `sudo docker run -p 49160:8080 --name some-name -d some-image` **-d means detached/runs in background, -p means map the host port 49160 to containers port 8080**



- `sudo docker run -p 49160:8080 --name some-name -d some-image`
- `sudo docker ps`
- `sudo docker ps -a`



**Docker Compose**

run `sudo docker-compose up` on the directory with the .yml file. Does the following:

- Runs the docker build command on this dir, which will have the below Dockerfile.
- Maps the ports
- Mount a volume to a directory on the machine
- Set up database with psql image
- Start up database first (depends_on)


docker-compose.yml:
```

version: '2'
services:
  web:
    build: .  
    ports:
      - "5000:5000"
    depends_on:
      - db 
    volumes:
      - ./webapp:/opt/webapp 
  db: 
    image: postgres:latest
    ports:
      - "5432:5432"

```

Dockerfile:

```
FROM node:carbon

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .

EXPOSE 8080
CMD [ "npm", "start" ]

```